package edu.eatmore.jjwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;

public class Runner {

  private static final char[] ALLOWED_CHARS =
         {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
          'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
          '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

  private static SecureRandom random = new SecureRandom();

  public static void main(String[] args) throws Exception {

    System.out.println("\n\nParsing keys...");

    String privateKeyFile = readFile("private.key");
    System.out.println("\nPrivate key file:");
    System.out.println(privateKeyFile);
    String publicKeyFile = readFile("public.key");
    System.out.println("\nPublic key file:");
    System.out.println(publicKeyFile);

    System.out.println("\n\nDecoding from Base64...");
    byte[] privateKeyDecoded = Base64.getDecoder().decode(privateKeyFile);
    System.out.println("Private key decoded. Lenght: " + privateKeyDecoded.length);
    byte[] publicKeyDecoded = Base64.getDecoder().decode(publicKeyFile);
    System.out.println("Public key decoded. Length: " + publicKeyDecoded.length);

    System.out.println("Compiling keys...");
    PrivateKey privateKey = KeyFactory.getInstance("EC").generatePrivate(new PKCS8EncodedKeySpec(privateKeyDecoded));
    System.out.println();
    System.out.println(privateKey);
    PublicKey publicKey = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(publicKeyDecoded));
    System.out.println(publicKey);

    System.out.println("Building JWT...");
    Calendar expire = Calendar.getInstance();
    expire.add(Calendar.HOUR, 3);
    System.out.println("Expire date is " + expire.getTime());
    System.out.println("Allowed JTI chars are " + Arrays.toString(ALLOWED_CHARS));
    String jwt = Jwts.builder()
        .setHeaderParam("alg", "ES256")
        .setHeaderParam("typ", "JWT")
        .setExpiration(expire.getTime())
        .claim("name", "Robert Token Man")
        .claim("scope", "self groups/admin")
        .setId(generateJti())
        .signWith(SignatureAlgorithm.ES256, privateKey)
        .compact();
    System.out.println("Generated JWT:");
    System.out.println(jwt);

    System.out.println("\nReading JWT...");
    Jws<Claims> claims = Jwts.parser()
        .setSigningKey(publicKey)
        .parseClaimsJws(jwt);
    System.out.println("Result:");
    System.out.println(claims);
  }

  private static String readFile(String fileName) throws IOException {
    return new String(Files.readAllBytes(Paths.get(fileName)));
  }

  private static String generateJti() {
    return new TokenBuilder()
        .appendRandomChars(8)
        .appendSeparator()
        .appendRandomChars(4)
        .appendSeparator()
        .appendRandomChars(4)
        .appendSeparator()
        .appendRandomChars(12)
        .toString();
  }


  private static class TokenBuilder {
    private StringBuilder result = new StringBuilder(31);

    public TokenBuilder appendRandomChars(int count) {
      for (int i = 0; i < count; i++) {
        result.append(ALLOWED_CHARS[random.nextInt(ALLOWED_CHARS.length)]);
      }
      return this;
    }

    public TokenBuilder appendSeparator() {
      result.append('-');
      return this;
    }

    @Override
    public String toString() {
      return result.toString();
    }
  }
}

